/**
* Image Annotation for Mootools 1.2.x
*
* @author Patrick Klaassen
* @copyright (c) 2010-2011 Patrick Klaassen
* @since 12-09-2010
* @version 1.0
* @license MIT-license
*/
ImageAnnotation = new Class({

	Implements: [Elements,Options],

	/**
	 * @type {Elements}
	 */
	element: undefined,

	/**
	 * @type {Object}
	 */
	options: {
		useAjax: false,
		ajaxAction: null,
		ajaxUrl: null,
		notes: new Array()
	},

	/**
	 * Init function
	 *
	 * @param {Element} imageElement
	 * @param {Object} options
	 * @returns void
	 */
	initialize: function(imageElement, options) {
		if ($chk(imageElement) == false) {
			// No element was passed in, so we need to stop the initialisation
		}

		// Merge default options with the options passed in
		this.setOptions(options);

		if (this.options.notes.length > 0) {
			this.notes = this.options.notes;
		}

		// Do the annotation
		return this.annotate(imageElement);
	},

	/**
	 * Make annotation of the image
	 *
	 * @param {Element} imageElement
	 * @return {Element} Annotated image as an element
	 */
	annotate: function(imageElement) {
		var el_div_image_annotation_annotations = new Element('div', {
			'class': 'image-annotation-annotations'
		});
		
		
		if ($chk(this.notes)) {
			var el_div_image_annotation_annotations = this.annotateUsingNotes(el_div_image_annotation_annotations);
		}
		
		if (this.options.useAjax == true) {
			var postObject = JSON.encode({
				action: this.options.ajaxAction
			});
			var notesRequest = new Request.JSON({
				url: this.options.ajaxUrl,
				data: {json: postObject},
				onComplete: this.annotateUsingAjax.bindWithEvent(this)
			}).send();
		}

		// Create container for the image and the annotation container
		var el_div_image_annotation_image_container = new Element('div', {
			'class': 'image-annotation-image-container'
		});
		el_div_image_annotation_image_container.wraps(imageElement);
		el_div_image_annotation_annotations.inject(el_div_image_annotation_image_container);

		var el_div_image_annotation_meta = this.createImageAnnotationMetaElement();

		var el_div_image_annotation_wrapper = new Element('div', {
			'class': 'image-annotation-wrapper'
		});

		el_div_image_annotation_meta.inject(el_div_image_annotation_wrapper);
		el_div_image_annotation_wrapper.wraps(el_div_image_annotation_image_container);
	},
	
	/**
	 * Do the annotation for each provided note
	 * 
	 * @see this.notes
	 * @param {Element} annotationContainer
	 * @returns {Element}
	 */
	annotateUsingNotes: function(annotationContainer) {
		// Loop for each note
		this.notes.each(function(obj, index) {
			var el_div_image_annotation_area = this.createAnnotationAreaElement(obj, index);
			var el_div_image_annotation_note = this.createAnnotationNoteElement(obj, index);

			annotationContainer.adopt(el_div_image_annotation_area, el_div_image_annotation_note);

			// Add hover event
			el_div_image_annotation_area.getElement('a').addEvents({
				'mouseover': function() {
					el_div_image_annotation_note.setStyle('display', 'block');
				},
				'mouseout': function() {
					el_div_image_annotation_note.setStyle('display', 'none');
				}
			});
		}, this);
		
		return annotationContainer;
	},
	
	annotateUsingAjax: function(JsonResponse) {
		if (!$chk(JsonResponse['success'])){
			//console.log(JsonResponse.message);
			return;
		}
		
	////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// hier verder
	},
	
	/**
	 * Creates an element of the annotation area
	 * 
	 * @param {Object} optionsObject
	 * @param {Integer} commentId
	 * @returns {Element}
	 */
	createAnnotationAreaElement: function(optionsObject, commentId) {
		var el_a = new Element('a', {
			'href': '#image-annotation-comment-' + (commentId + 1),
			'styles': {
				'width': optionsObject.width,
				'height': optionsObject.height
			}
		});

		var el_div_image_annotation_area = new Element('div', {
			'class': 'image-annotation-area',
			'styles': {
				'left': optionsObject.left,
				'top': optionsObject.top
			}
		});
		
		el_a.inject(el_div_image_annotation_area);
		
		return el_div_image_annotation_area;
	},
	
	/**
	 * Creates the annotation note element
	 * 
	 * @param {Object} optionsObject
	 * @param {Integer} commentId
	 * @returns {Element}
	 */
	createAnnotationNoteElement: function(optionsObject, commentId) {
		var el_div_image_annotation_note = new Element('div', {
			'class': 'image-annotation-note',
			'styles': {
				'left': optionsObject.left,
				'top': (optionsObject.top + optionsObject.height) + 10
			}
		});

		var el_div_image_annotation_note_meta = this.createAnnotationNoteMetaElement(commentId);
		
		var el_div_image_annotation_note_text = new Element('div', {
			'class': 'image-annotation-note-text',
			'html': optionsObject.text
		});
		
		el_div_image_annotation_note.adopt(el_div_image_annotation_note_meta, el_div_image_annotation_note_text);
		
		return el_div_image_annotation_note;
	},
	
	/**
	 * Creates the annotation note meta element
	 * 
	 * @param {Integer} commentId
	 * @returns {Element}
	 */
	createAnnotationNoteMetaElement: function(commentId) {
		var el_div_image_annotation_note_meta = new Element('div', {
			'class': 'image-annotation-note-meta'
		});

		var el_ul_image_annotation_note_meta = new Element('ul');

		// Print commentId
		var el_li = new Element('li', {
			'html': '#' + (commentId + 1)
		});
		el_li.inject(el_ul_image_annotation_note_meta);

		// Print separator
		var el_li_separator = this.createSeparatorElement();
		el_li_separator.inject(el_ul_image_annotation_note_meta);

		// Print author if using ajax
		if (this.options.useAjax == true) {
			var el_li = new Element('li', {
				'html': 'Author: PMK'
			});
			el_li.inject(el_ul_image_annotation_note_meta);
			
			// Print separator
			var el_li_separator = this.createSeparatorElement();
			el_li_separator.inject(el_ul_image_annotation_note_meta);
		}

		// Print text 'click to reply/read more'
		var el_li = new Element('li', {
			'html': 'Click to reply/read more.'
		});
		el_li.inject(el_ul_image_annotation_note_meta);

		el_ul_image_annotation_note_meta.inject(el_div_image_annotation_note_meta);
		
		return el_div_image_annotation_note_meta;
	},
	
	/**
	 * Creates the separator element
	 * 
	 * @returns {Element}
	 */
	createSeparatorElement: function() {
		return new Element('li', {
			'html': '&nbsp;|&nbsp;'
		});
	},
	
	/**
	 * Creates the image annotation meta element
	 * 
	 * @returns {Element}
	 */
	createImageAnnotationMetaElement: function() {
		var el_div_image_annotation_meta = new Element('div', {
			'class': 'image-annotation-meta'
		});
		
		var el_a = new Element('a', {
			'title': 'Jump to image #',
			'href': '#image-annotation-',
			'html': '#'
		});
		var el_li_id = new Element('li');
		el_a.inject(el_li_id);

		// Create 'sharing image' element
		var el_li_share = new Element('li');
		el_li_share.innerHTML = addThis();
		
		var el_li_mouseover = new Element('li', {
			'html': 'Mouseover to load notes.'
		});
		
		var el_ul_image_annotation_meta = new Element('ul');
		el_ul_image_annotation_meta.adopt(el_li_id, this.createSeparatorElement(), el_li_share, this.createSeparatorElement(), el_li_mouseover);

		el_ul_image_annotation_meta.inject(el_div_image_annotation_meta);
		
		return el_div_image_annotation_meta;
	}
	
});

/**
 * Returns the generated AddThis HTML
 * 
 * @see http://www.addthis.com
 * @returns {String}
 */
function addThis() {
	return ('<!-- AddThis Button BEGIN --><div class="addthis_toolbox addthis_default_style"><a class="addthis_button_preferred_1"></a><a class="addthis_button_preferred_2"></a><a class="addthis_button_preferred_3"></a><a class="addthis_button_preferred_4"></a><a class="addthis_button_compact"></a></div><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c9531696da6da84"></script><!-- AddThis Button END -->');
}
